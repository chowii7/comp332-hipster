package compiler

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
  * Created by chowii on 8/10/17.
  */
@RunWith(classOf[JUnitRunner])
class ForLoopTypeAnalysisTest extends TypeAnalysisTests {

    //=============== For Statement Pass ===============
    test("test for statement int to int pass"){
      val message =
        semanticTestInline(
          """
            |dimension(100,100);
            |neighbourhood N = [0,1];
            |state {}
            |updater {}
            |mapper{
            |return (0);
            |}
            |function test(){
            |int x = 0;
            | for i=5 to 10
            |    x = i + 1;
            |}
          """.stripMargin)
      assert(message.length === 0, "expecting no error")
    }

    test("test for statement int to int step int pass"){
      val message =
        semanticTestInline(
          """
            |dimension(100,100);
            |neighbourhood N = [0,1];
            |state {}
            |updater {}
            |mapper{
            |return (0);
            |}
            |function test(){
            |int x = 0;
            | for i=5 to 100 step 2
            |    x = i + 1;
            |}
          """.stripMargin)
      assert(message.length === 0, "expecting no error")
    }

    //=============== For Statement Fail ===============
    test("test for statement float to int step int fail"){
      val message =
        semanticTestInline(
          """
            |dimension(100,100);
            |neighbourhood N = [0,1];
            |state {}
            |updater {}
            |mapper{
            |return (0);
            |}
            |function test(){
            |int x = 0;
            | for i=5.0 to 100 step 2
            |    x = i + 1;
            |}
          """.stripMargin)
      assert(message.length === 1, "expecting no error")
      assertMessage(message, 0, 11, 8, "type error, expecting 'int' found 'float'")
    }

    test("test for statement float to float step int fail"){
      val message =
        semanticTestInline(
          """
            |dimension(100,100);
            |neighbourhood N = [0,1];
            |state {}
            |updater {}
            |mapper{
            |return (0);
            |}
            |function test(){
            |int x = 0;
            |for i=5.0 to 100.0 step 2
            |    x = i + 1;
            |}
          """.stripMargin)
      assert(message.length === 2, "expecting no error")
      assertMessage(message, 0, 11, 7, "type error, expecting 'int' found 'float'")
      assertMessage(message, 1, 11, 14, "type error, expecting 'int' found 'float'")
    }

    test("test for statement float to float step float fail"){
      val message =
        semanticTestInline(
          """
            |dimension(100,100);
            |neighbourhood N = [0,1];
            |state {}
            |updater {}
            |mapper{
            |return (0);
            |}
            |function test(){
            |int x = 0;
            |for i=5.0 to 100.0 step 2.0
            |    x = i + 1;
            |}
          """.stripMargin)
      assert(message.length === 3, "expecting no error")
      assertMessage(message, 0, 11, 7, "type error, expecting 'int' found 'float'")
      assertMessage(message, 1, 11, 14, "type error, expecting 'int' found 'float'")
      assertMessage(message, 2, 11, 25, "type error, expecting 'int' found 'float'")
    }

    test("test for statement boolean to int fail"){
      val message =
        semanticTestInline(
          """
            |dimension(100,100);
            |neighbourhood N = [0,1];
            |state {}
            |updater {}
            |mapper{
            |return (0);
            |}
            |function test(){
            |int x = 0;
            |for i = true to 100
            |    x = i + 1;
            |}
          """.stripMargin)
      assert(message.length === 1, "expecting no error")
      assertMessage(message, 0, 11, 9, "type error, expecting 'int' found 'boolean'")
    }

    test("test for statement boolean to boolean fail"){
      val message =
        semanticTestInline(
          """
            |dimension(100,100);
            |neighbourhood N = [0,1];
            |state {}
            |updater {}
            |mapper{
            |return (0);
            |}
            |function test(){
            |int x = 0;
            |for i = true to true
            |    x = i + 1;
            |}
          """.stripMargin)
      assert(message.length === 2, "expecting no error")
      assertMessage(message, 0, 11, 9, "type error, expecting 'int' found 'boolean'")
      assertMessage(message, 1, 11, 17, "type error, expecting 'int' found 'boolean'")
    }

    test("test for statement boolean to boolean step int fail"){
      val message =
        semanticTestInline(
          """
            |dimension(100,100);
            |neighbourhood N = [0,1];
            |state {}
            |updater {}
            |mapper{
            |return (0);
            |}
            |function test(){
            |int x = 0;
            |for i = true to true step 2
            |    x = i + 1;
            |}
          """.stripMargin)
      assert(message.length === 2, "expecting no error")
      assertMessage(message, 0, 11, 9, "type error, expecting 'int' found 'boolean'")
      assertMessage(message, 1, 11, 17, "type error, expecting 'int' found 'boolean'")
    }

    test("test for statement boolean to boolean step boolean fail"){
      val message =
        semanticTestInline(
          """
            |dimension(100,100);
            |neighbourhood N = [0,1];
            |state {}
            |updater {}
            |mapper{
            |return (0);
            |}
            |function test(){
            |int x = 0;
            |for i = true to true step true
            |    x = i + 1;
            |}
          """.stripMargin)
      assert(message.length === 3, "expecting no error")
      assertMessage(message, 0, 11, 9, "type error, expecting 'int' found 'boolean'")
      assertMessage(message, 1, 11, 17, "type error, expecting 'int' found 'boolean'")
      assertMessage(message, 2, 11, 27, "type error, expecting 'int' found 'boolean'")
    }

    test("test for statement neighbour to int fail"){
      val message =
        semanticTestInline(
          """
            |dimension(100,100);
            |neighbourhood N = [0,1];
            |state {}
            |updater {}
            |mapper{
            |return (0);
            |}
            |function test(){
            |int x = 0;
            |for i = N to 100
            |    x = i + 1;
            |}
          """.stripMargin)
      assert(message.length === 1, "expecting no error")
      assertMessage(message, 0, 11, 9, "type error, expecting 'int' found 'neighbour'")
    }

    test("test for statement neighbour to neighbour fail"){
      val message =
        semanticTestInline(
          """
            |dimension(100,100);
            |neighbourhood N = [0,1];
            |state {}
            |updater {}
            |mapper{
            |return (0);
            |}
            |function test(){
            |int x = 0;
            |for i = N to N
            |    x = i + 1;
            |}
          """.stripMargin)
      assert(message.length === 2, "expecting no error")
      assertMessage(message, 0, 11, 9, "type error, expecting 'int' found 'neighbour'")
      assertMessage(message, 1, 11, 14, "type error, expecting 'int' found 'neighbour'")
    }

    test("test for statement neighbour to neighbour step int fail"){
      val message =
        semanticTestInline(
          """
            |dimension(100,100);
            |neighbourhood N = [0,1];
            |state {}
            |updater {}
            |mapper{
            |return (0);
            |}
            |function test(){
            |int x = 0;
            |for i = N to N step 2
            |    x = i + 1;
            |}
          """.stripMargin)
      assert(message.length === 2, "expecting no error")
      assertMessage(message, 0, 11, 9, "type error, expecting 'int' found 'neighbour'")
      assertMessage(message, 1, 11, 14, "type error, expecting 'int' found 'neighbour'")
    }

    test("test for statement neighbour to neighbour step neighbour fail"){
      val message =
        semanticTestInline(
          """
            |dimension(100,100);
            |neighbourhood N = [0,1];
            |state {}
            |updater {}
            |mapper{
            |return (0);
            |}
            |function test(){
            |int x = 0;
            |for i = N to N step N
            |    x = i + 1;
            |}
          """.stripMargin)
      assert(message.length === 3, "expecting no error")
      assertMessage(message, 0, 11, 9, "type error, expecting 'int' found 'neighbour'")
      assertMessage(message, 1, 11, 14, "type error, expecting 'int' found 'neighbour'")
      assertMessage(message, 2, 11, 21, "type error, expecting 'int' found 'neighbour'")
    }

    test("test for statement neighbour to float fail"){
      val message =
        semanticTestInline(
          """
            |dimension(100,100);
            |neighbourhood N = [0,1];
            |state {}
            |updater {}
            |mapper{
            |return (0);
            |}
            |function test(){
            |int x = 0;
            |for i = N to 100.0
            |    x = i + 1;
            |}
          """.stripMargin)
      assert(message.length === 2, "expecting no error")
      assertMessage(message, 0, 11, 9, "type error, expecting 'int' found 'neighbour'")
      assertMessage(message, 1, 11, 14, "type error, expecting 'int' found 'float'")
    }

    test("test for statement neighbour to float step int fail"){
      val message =
        semanticTestInline(
          """
            |dimension(100,100);
            |neighbourhood N = [0,1];
            |state {}
            |updater {}
            |mapper{
            |return (0);
            |}
            |function test(){
            |int x = 0;
            |for i = N to 100.0 step 2
            |    x = i + 1;
            |}
          """.stripMargin)
      assert(message.length === 2, "expecting no error")
      assertMessage(message, 0, 11, 9, "type error, expecting 'int' found 'neighbour'")
      assertMessage(message, 1, 11, 14, "type error, expecting 'int' found 'float'")
    }


    test("test for statement neighbour to float step neighbour fail"){
      val message =
        semanticTestInline(
          """
            |dimension(100,100);
            |neighbourhood N = [0,1];
            |state {}
            |updater {}
            |mapper{
            |return (0);
            |}
            |function test(){
            |int x = 0;
            |for i = N to 100.0 step N
            |    x = i + 1;
            |}
          """.stripMargin)
      assert(message.length === 3, "expecting no error")
      assertMessage(message, 0, 11, 9, "type error, expecting 'int' found 'neighbour'")
      assertMessage(message, 1, 11, 14, "type error, expecting 'int' found 'float'")
      assertMessage(message, 2, 11, 25, "type error, expecting 'int' found 'neighbour'")
    }

    test("test for statement int to neighbour fail"){
      val message =
        semanticTestInline(
          """
            |dimension(100,100);
            |neighbourhood N = [0,1];
            |state {}
            |updater {}
            |mapper{
            |return (0);
            |}
            |function test(){
            |int x = 0;
            |for i = 5 to N
            |    x = i + 1;
            |}
          """.stripMargin)
      assert(message.length === 1, "expecting no error")
      assertMessage(message, 0, 11, 14, "type error, expecting 'int' found 'neighbour'")
    }

    test("test for statement neighbour to int step float fail"){
      val message =
        semanticTestInline(
          """
            |dimension(100,100);
            |neighbourhood N = [0,1];
            |state {}
            |updater {}
            |mapper{
            |return (0);
            |}
            |function test(){
            |int x = 0;
            |for i = N to 100 step 2.0
            |    x = i + 1;
            |}
          """.stripMargin)
      assert(message.length === 2, "expecting no error")
      assertMessage(message, 0, 11, 9, "type error, expecting 'int' found 'neighbour'")
      assertMessage(message, 1, 11, 23, "type error, expecting 'int' found 'float'")
    }

    test("test for statement neighbour to float step float fail"){
      val message =
        semanticTestInline(
          """
            |dimension(100,100);
            |neighbourhood N = [0,1];
            |state {}
            |updater {}
            |mapper{
            |return (0);
            |}
            |function test(){
            |int x = 0;
            |for i = N to 100.0 step 2.0
            |    x = i + 1;
            |}
          """.stripMargin)
      assert(message.length === 3, "expecting no error")
      assertMessage(message, 0, 11, 9, "type error, expecting 'int' found 'neighbour'")
      assertMessage(message, 1, 11, 14, "type error, expecting 'int' found 'float'")
      assertMessage(message, 2, 11, 25, "type error, expecting 'int' found 'float'")
    }
}
