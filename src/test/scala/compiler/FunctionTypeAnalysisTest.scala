package compiler

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
  * Created by chowii on 8/10/17.
  */

@RunWith(classOf[JUnitRunner])
class FunctionTypeAnalysisTest extends TypeAnalysisTests {

  //=============== Function Statement Pass ===============
  test("test function return int pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): int{
          | return (2);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test function return int with if pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): int{
          | if 6 > 5 then return (1); else return (2);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test function return float pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): float{
          | return (2.0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test function return float with if pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): float{
          | if 6 > 5 then return (1.0); else return (2.0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test function return neighbour pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): neighbour{
          | return (N);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test function return neighbour with if pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): neighbour{
          | if 6 > 5 then return (N); else return (N);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test function return boolean pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): boolean{
          | return (true);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test function return boolean with if then return true else false pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): boolean{
          | if 6 > 5 then return (true); else return (false);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test function return boolean false pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): boolean{
          | return (false);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test function return boolean with if then return false else false pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): boolean{
          | if 6 > 5 then return (false); else return (false);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test function return boolean with if then return false else true pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): boolean{
          | if 6 > 5 then return (false); else return (true);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  //=============== Function Statement Fail ===============
  test("test function return int with no return fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): int{ }
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 9, 1, "missing `return` in body of function declaration")
  }

  test("test function return float with no return fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): float{ }
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 9, 1, "missing `return` in body of function declaration")
  }

  test("test function return boolean with no return fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): boolean{ }
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 9, 1, "missing `return` in body of function declaration")
  }

  test("test function return neighbour with no return fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): neighbour{ }
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 9, 1, "missing `return` in body of function declaration")
  }

  test("test function return int with float return fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): int{
          |   return (1.0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 10, 12, "type error, expecting 'int' found 'float'")
  }

  test("test function return int with boolean return fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): int{
          |   return (true);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 10, 12, "type error, expecting 'int' found 'boolean'")
  }

  test("test function return int with neighbour return fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): int{
          |   return (N);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 10, 12, "type error, expecting 'int' found 'neighbour'")
  }

  test("test function return float with boolean return fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): float{
          |   return (true);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 10, 12, "type error, expecting 'int' or 'float' found 'boolean'")
  }

  test("test function return float with neighbour return fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): float{
          |   return (N);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 10, 12, "type error, expecting 'int' or 'float' found 'neighbour'")
  }

  test("test function return boolean with int return fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): boolean{
          |   return (1);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 10, 12, "type error, expecting 'boolean' found 'int'")
  }

  test("test function return boolean with float return fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): boolean{
          |   return (1.0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 10, 12, "type error, expecting 'boolean' found 'float'")
  }

  test("test function return boolean with neighbour return fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): boolean{
          |   return (N);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 10, 12, "type error, expecting 'boolean' found 'neighbour'")
  }

  test("test function return neighbour with int return fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): neighbour{
          |   return (1);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 10, 12, "type error, expecting 'neighbour' found 'int'")
  }

  test("test function return neighbour with float return fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): neighbour{
          |   return (1.0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 10, 12, "type error, expecting 'neighbour' found 'float'")
  }

  test("test function return neighbour with boolean return fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (0);
          |}
          |function test(): neighbour{
          |   return (true);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 10, 12, "type error, expecting 'neighbour' found 'boolean'")
  }

}
