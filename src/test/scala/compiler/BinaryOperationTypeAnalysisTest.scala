package compiler

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
  * Created by chowii on 7/10/17.
  */
@RunWith(classOf[JUnitRunner])
class BinaryOperationTypeAnalysisTest extends TypeAnalysisTests {

  //Compute Arithmetic type Pass
  //=============== Plus Expression ===============
  test("test float add int results in float"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |float x = 1.0;
          |int y = 1;
          |float z = x + y;
          |return (y);
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test int add int with int result"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |   int x = 10;
          |   int y = 10;
          |   int z = x + y;
          |   return (z);
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test int add int with float result"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |   int x = 10;
          |   int y = 10;
          |   float z = x + y;
          |   return (y);
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test float add float with float result"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |   float x = 10.0;
          |   float y = 1.0;
          |   float z = x + y;
          |   return (0);
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  //=============== Multiply expression ===============
  test("test float multiply int results in float"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |float x = 1.0;
          |int y = 1;
          |float z = x * y;
          |return (y);
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test int multiply int with int result"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |   int x = 10;
          |   int y = 10;
          |   int z = x * y;
          |   return (z);
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test int multiply int with float result"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |   int x = 10;
          |   int y = 10;
          |   float z = x * y;
          |   return (y);
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test float multiply float with float result"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |   float x = 10.0;
          |   float y = 1.0;
          |   float z = x * y;
          |   return (0);
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  //=============== Divide expression ===============
  test("test float divide int results in float"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |float x = 1.0;
          |int y = 1;
          |float z = x / y;
          |return (y);
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test int divide int with int result"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |   int x = 10;
          |   int y = 10;
          |   int z = x / y;
          |   return (z);
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test int divide int with float result"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |   int x = 10;
          |   int y = 10;
          |   float z = x / y;
          |   return (y);
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test float divide float with float result"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |   float x = 10.0;
          |   float y = 1.0;
          |   float z = x / y;
          |   return (0);
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  //=============== Minus expression ===============
  test("test float minus int results in float"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |float x = 1.0;
          |int y = 1;
          |float z = x - y;
          |return (y);
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test int minus int with int result"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |   int x = 10;
          |   int y = 10;
          |   int z = x - y;
          |   return (z);
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test int minus int with float result"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |   int x = 10;
          |   int y = 10;
          |   float z = x - y;
          |   return (y);
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test float minus float with float result"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |   float x = 10.0;
          |   float y = 1.0;
          |   float z = x - y;
          |   return (0);
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  //=============== Modulus expression ===============
  test("test float modulus int results in float"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |float x = 1.0;
          |int y = 1;
          |float z = x % y;
          |return (y);
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test int modulus int with int result"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |   int x = 10;
          |   int y = 10;
          |   int z = x % y;
          |   return (z);
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test int modulus int with float result"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |   int x = 10;
          |   int y = 10;
          |   float z = x % y;
          |   return (y);
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test float modulus float with float result"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |   float x = 10.0;
          |   float y = 1.0;
          |   float z = x % y;
          |   return (0);
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  //=============== Compute Arithmetic Type Fail ===============
  //=============== Plus expression ===============
  test("test float plus int errors with int result"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |float x = 1.0;
          |int y = 1;
          |int z = x + y;
          |return (z);
          |}
        """.stripMargin)
    assert(message.length == 1, "type error, expecting 'int' found 'float'")
    assertMessage(message,0, 9, 9, "type error, expecting 'int' found 'float'")
  }

  test("test int plus float errors with int result"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int x = 10;
          |float y = 1.0;
          |int z = x + y;
          |return (z);
          |}
        """.stripMargin)
    assert(message.length == 1, "expecting  one error")
    assertMessage(message,0, 9, 9, "type error, expecting 'int' found 'float'")
  }

  test("test boolean plus float results errors "){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean x = true;
          |float y = 1.0;
          |int z = x + y;
          |return (0);
          |}
        """.stripMargin)
    assert(message.length == 1, "expecting  one error")
    assertMessage(message,0, 9, 9, "type error, expecting 'int' or 'float' found 'boolean'")
  }

  //=============== Multiply expression ===============
  test("test float multiply int errors with int result"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |float x = 1.0;
          |int y = 1;
          |int z = x * y;
          |return (z);
          |}
        """.stripMargin)
    assert(message.length == 1, "type error, expecting 'int' found 'float'")
    assertMessage(message,0, 9, 9, "type error, expecting 'int' found 'float'")
  }

  test("test int multiply float errors with int result"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int x = 10;
          |float y = 1.0;
          |int z = x * y;
          |return (z);
          |}
        """.stripMargin)
    assert(message.length == 1, "expecting  one error")
    assertMessage(message,0, 9, 9, "type error, expecting 'int' found 'float'")
  }

  test("test boolean multiply float results errors "){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean x = true;
          |float y = 1.0;
          |int z = x * y;
          |return (0);
          |}
        """.stripMargin)
    assert(message.length == 1, "expecting  one error")
    assertMessage(message,0, 9, 9, "type error, expecting 'float' or 'int' found 'boolean'")
  }

  test("test neighbour multiply float results errors "){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int z = N * 1.0;
          |return (0);
          |}
        """.stripMargin)
    assert(message.length == 1, "expecting  one error")
    assertMessage(message,0, 7, 9, "type error, expecting 'float' or 'int' found 'neighbour'")
  }

  //=============== Divide expression ===============
  test("test float divide int errors with int result"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |float x = 1.0;
          |int y = 1;
          |int z = x / y;
          |return (z);
          |}
        """.stripMargin)
    assert(message.length == 1, "type error, expecting 'int' found 'float'")
    assertMessage(message,0, 9, 9, "type error, expecting 'int' found 'float'")
  }

  test("test int divide float errors with int result"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int x = 10;
          |float y = 1.0;
          |int z = x / y;
          |return (z);
          |}
        """.stripMargin)
    assert(message.length == 1, "expecting  one error")
    assertMessage(message,0, 9, 9, "type error, expecting 'int' found 'float'")
  }

  test("test boolean divide float results errors "){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean x = true;
          |float y = 1.0;
          |int z = x / y;
          |return (0);
          |}
        """.stripMargin)
    assert(message.length == 1, "expecting  one error")
    assertMessage(message,0, 9, 9, "type error, expecting 'float' or 'int' found 'boolean'")
  }

  test("test neighbour divide float results errors "){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int z = N / 1.0;
          |return (0);
          |}
        """.stripMargin)
    assert(message.length == 1, "expecting  one error")
    assertMessage(message,0, 7, 9, "type error, expecting 'float' or 'int' found 'neighbour'")
  }

  //=============== Minus expression ===============
  test("test float minus int errors with int result"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |float x = 1.0;
          |int y = 1;
          |int z = x - y;
          |return (z);
          |}
        """.stripMargin)
    assert(message.length == 1, "type error, expecting 'int' found 'float'")
    assertMessage(message,0, 9, 9, "type error, expecting 'int' found 'float'")
  }

  test("test int minus float errors with int result"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int x = 10;
          |float y = 1.0;
          |int z = x - y;
          |return (z);
          |}
        """.stripMargin)
    assert(message.length == 1, "expecting  one error")
    assertMessage(message,0, 9, 9, "type error, expecting 'int' found 'float'")
  }

  test("test boolean minus float results errors "){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean x = true;
          |float y = 1.0;
          |int z = x - y;
          |return (0);
          |}
        """.stripMargin)
    assert(message.length == 1, "expecting  one error")
    assertMessage(message,0, 9, 9, "type error, expecting 'float' or 'int' found 'boolean'")
  }

  test("test neighbour minus float results errors "){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int z = N - 1.0;
          |return (0);
          |}
        """.stripMargin)
    assert(message.length == 1, "expecting  one error")
    assertMessage(message,0, 7, 9, "type error, expecting 'float' or 'int' found 'neighbour'")
  }

  //=============== Modulus expression ===============
  test("test float modulus int errors with int result"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |float x = 1.0;
          |int y = 1;
          |int z = x % y;
          |return (z);
          |}
        """.stripMargin)
    assert(message.length == 1, "type error, expecting 'int' found 'float'")
    assertMessage(message,0, 9, 9, "type error, expecting 'int' found 'float'")
  }

  test("test int modulus float errors with int result"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int x = 10;
          |float y = 1.0;
          |int z = x % y;
          |return (z);
          |}
        """.stripMargin)
    assert(message.length == 1, "expecting  one error")
    assertMessage(message,0, 9, 9, "type error, expecting 'int' found 'float'")
  }

  test("test boolean modulus float results errors "){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean x = true;
          |float y = 1.0;
          |int z = x % y;
          |return (0);
          |}
        """.stripMargin)
    assert(message.length == 1, "expecting  one error")
    assertMessage(message,0, 9, 9, "type error, expecting 'float' or 'int' found 'boolean'")
  }

  test("test neighbour modulus float results errors "){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int z = N % 1.0;
          |return (0);
          |}
        """.stripMargin)
    assert(message.length == 1, "expecting  one error")
    assertMessage(message,0, 7, 9, "type error, expecting 'float' or 'int' found 'neighbour'")
  }

  //=============== Compute Boolean Type Pass ===============
  //=============== Or expression ===============
  test("test boolean OR results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = true || false;
          |return (0);
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  //=============== And expression ===============
  test("test boolean And results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = true && false;
          |return (0);
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  //=============== Not expression ===============
  test("test boolean Not results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = !true;
          |return (0);
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  //=============== Compute Boolean Type Fail ===============
  //=============== Or expression ===============
  test("test boolean Or int results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = true || 5;
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 21, "type error, expecting 'boolean' found 'int'")
  }

  test("test boolean Or float results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = true || 5.0;
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 21, "type error, expecting 'boolean' found 'float'")
  }

  test("test boolean Or neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = true || N;
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 21, "type error, expecting 'boolean' found 'neighbour'")
  }

  //=============== And expression ===============
  test("test boolean and int results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = true && 5;
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 21, "type error, expecting 'boolean' found 'int'")
  }

  test("test boolean and float results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = true && 5.0;
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 21, "type error, expecting 'boolean' found 'float'")
  }

  test("test boolean and neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = true && N;
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 21, "type error, expecting 'boolean' found 'neighbour'")
  }

  //=============== Not expression ===============
  test("test boolean not int results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = !5;
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 14, "type error, expecting 'boolean' found 'int'")
  }

  test("test boolean not float results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = !5.0;
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 14, "type error, expecting 'boolean' found 'float'")
  }

  test("test boolean not neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = !N;
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 14, "type error, expecting 'boolean' found 'neighbour'")
  }

  //=============== Compute Equal Type Pass ===============
  test("test equal int and int results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5 == 5);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test equal float and float results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5.0 == 5.0);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test equal int and float results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5 == 5.0);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test equal float and int results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5 == 5.0);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test equal boolean true and boolean true results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (true == true);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test equal boolean true and boolean false results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (true == false);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test equal neighbour and neighbour results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (N == N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test equal neighbour and with different neighbour results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1], S = [1,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (N == S);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  //=============== Compute Equal Type Fail ===============

  test("test equal int and neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5 == N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 19, "type error, expecting 'int' or 'float' found 'neighbour'")
  }

  test("test equal float and neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5.0 == N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 21, "type error, expecting 'int' or 'float' found 'neighbour'")
  }

  test("test equal boolean true and neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (true == N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 22, "type error, expecting 'boolean' found 'neighbour'")
  }

  test("test equal boolean false and neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (false == N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting one error")
    assertMessage(message, 0, 7, 23, "type error, expecting 'boolean' found 'neighbour'")
  }

  //=============== Compute Relational Type Pass ===============
  //=============== GreaterEqual Expression ===============
  test("test greaterEqual int and boolean true results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5 >= true);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test greaterEqual boolean true and int results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5 >= true);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting one error")
  }

  test("test greaterEqual int and int results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5 >= 4);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test greaterEqual float and float results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5.0 >= 4.0);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test greaterEqual float and int results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5.0 >= 4);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test greaterEqual int and float results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5 >= 4.0);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  //=============== LessEqual Expression ===============
  test("test lessEqual int and boolean true results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5 <= true);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test lessEqual boolean true and int results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5 <= true);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting one error")
  }

  test("test lessEqual int and int results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5 <= 4);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test lessEqual float and float results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5.0 <= 4.0);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test lessEqual float and int results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5.0 <= 4);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test lessEqual int and float results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5 <= 4.0);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  //=============== Greater Expression ===============
  test("test greater int and boolean true results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5 > true);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test greater boolean true and int results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5 > true);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting one error")
  }

  test("test greater int and int results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5 > 4);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test greater float and float results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5.0 > 4.0);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test greater float and int results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5.0 > 4);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test greater int and float results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5 > 4.0);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  //=============== Less Expression ===============
  test("test less int and boolean true results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5 < true);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test less boolean true and int results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5 < true);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting one error")
  }

  test("test less int and int results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5 < 4);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test less float and float results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5.0 < 4.0);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test less float and int results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5.0 < 4);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  test("test less int and float results pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (5 < 4.0);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  //=============== Compute Relational Type Fail ===============
  //=============== GreaterEqual Expression ===============
  test("test greaterEqual neighbour and boolean true results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (N >= true);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 14, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greaterEqual boolean true and neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (true >= N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 22, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greaterEqual neighbour and boolean false results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (N >= false);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 14, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greaterEqual boolean false and neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (false >= N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 23, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greaterEqual neighbour and neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (N >= N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 2, "expecting no error")
    assertMessage(message, 0, 7, 14, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greaterEqual neighbour and int results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (N >= 1);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 14, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greaterEqual int and neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (1 >= N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 19, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greaterEqual neighbour and float results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (N >= 1.0);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 14, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greaterEqual float and neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (1.0 >= N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 21, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greaterEqual neighbour and boolean true results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (N >= true);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 10, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greaterEqual boolean true and neighbour results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (true >= N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 18, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greaterEqual neighbour and boolean false results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (N >= false);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 10, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greaterEqual boolean false and neighbour results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (false >= N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 19, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greaterEqual neighbour and neighbour results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (N >= N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 2, "expecting no error")
    assertMessage(message, 0, 7, 10, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greaterEqual neighbour and int results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (N >= 1);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 10, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greaterEqual int and neighbour results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (1 >= N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 15, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greaterEqual neighbour and float results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (N >= 1.0);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 10, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greaterEqual float and neighbour results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (1.0 >= N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 17, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  //=============== LessEqual Expression ===============
  test("test lessEqual neighbour and boolean true results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (N <= true);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 14, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test lessEqual boolean true and neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (true <= N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 22, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test lessEqual neighbour and boolean false results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (N <= false);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 14, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test lessEqual boolean false and neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (false <= N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 23, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test lessEqual neighbour and neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (N <= N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 2, "expecting no error")
    assertMessage(message, 0, 7, 14, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test lessEqual neighbour and int results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (N <= 1);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 14, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test lessEqual int and neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (1 <= N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 19, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test lessEqual neighbour and float results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (N <= 1.0);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 14, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test lessEqual float and neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (1.0 <= N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 21, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test lessEqual neighbour and boolean true results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (N <= true);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 10, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test lessEqual boolean true and neighbour results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (true <= N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 18, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test lessEqual neighbour and boolean false results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (N <= false);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 10, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test lessEqual boolean false and neighbour results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (false <= N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 19, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test lessEqual neighbour and neighbour results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (N <= N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 2, "expecting no error")
    assertMessage(message, 0, 7, 10, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test lessEqual neighbour and int results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (N <= 1);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 10, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test lessEqual int and neighbour results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (1 <= N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 15, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test lessEqual neighbour and float results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (N <= 1.0);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 10, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test lessEqual float and neighbour results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (1.0 <= N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 17, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  //=============== Greater Expression ===============
  test("test greater neighbour and boolean true results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (N > true);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 14, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greater boolean true and neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (true > N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 21, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greater neighbour and boolean false results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (N > false);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 14, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greater boolean false and neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (false > N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 22, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greater neighbour and neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (N > N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 2, "expecting no error")
    assertMessage(message, 0, 7, 14, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greater neighbour and int results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (N > 1);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 14, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greater int and neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (1 > N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 18, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greater neighbour and float results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (N > 1.0);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 14, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greater float and neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (1.0 > N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 20, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greater neighbour and boolean true results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (N > true);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 10, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greater boolean true and neighbour results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (true > N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 17, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greater neighbour and boolean false results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (N > false);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 10, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greater boolean false and neighbour results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (false > N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 18, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greater neighbour and neighbour results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (N > N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 2, "expecting no error")
    assertMessage(message, 0, 7, 10, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greater neighbour and int results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (N > 1);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 10, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greater int and neighbour results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (1 > N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 14, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greater neighbour and float results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (N > 1.0);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 10, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test greater float and neighbour results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (1.0 > N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 16, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  //=============== Less Expression ===============
  test("test less neighbour and boolean true results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (N <  true);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 14, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test less boolean true and neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (true <  N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 22, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test less neighbour and boolean false results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (N <  false);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 14, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test less boolean false and neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (false <  N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 23, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test less neighbour and neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (N <  N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 2, "expecting no error")
    assertMessage(message, 0, 7, 14, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test less neighbour and int results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (N <  1);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 14, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test less int and neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (1 <  N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 19, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test less neighbour and float results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (N <  1.0);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 14, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test less float and neighbour results fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |boolean y = (1.0 <  N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 21, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test less neighbour and boolean true results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (N <  true);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 10, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test less boolean true and neighbour results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (true <  N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 18, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test less neighbour and boolean false results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (N <  false);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 10, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test less boolean false and neighbour results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (false <  N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 19, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test less neighbour and neighbour results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (N <  N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 2, "expecting no error")
    assertMessage(message, 0, 7, 10, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test less neighbour and int results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (N <  1);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 10, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test less int and neighbour results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (1 <  N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 15, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test less neighbour and float results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (N <  1.0);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 10, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

  test("test less float and neighbour results to int fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |int y = (1.0 <  N);
          |return (0);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting no error")
    assertMessage(message, 0, 7, 17, "type error, expecting 'int' or 'float' or 'boolean' found 'neighbour'")
  }

}
