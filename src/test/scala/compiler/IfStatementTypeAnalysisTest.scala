package compiler

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
  * Created by chowii on 8/10/17.
  */
@RunWith(classOf[JUnitRunner])
class IfStatementTypeAnalysisTest extends TypeAnalysisTests{

  //=============== If Statement Pass ===============
  test("test if statement true"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = true;
          | if true then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement false"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = true;
          | if false then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with boolean identifier true"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = true;
          | if x then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with boolean identifier false"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if x then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  //=============== Boolean Operation Pass ===============
  //=============== And Operation Pass ===============
  test("test if statement with `and` operator true && true"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if true && true then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with `and` operator true && false"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if true && false then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with `and` operator false && false"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if false && false then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with `and` operator false && true"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if false && true then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  //=============== Not-And Operation Pass ===============
  test("test if statement with `and` operator not true && true"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if !true && true then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with `and` operator true && not true"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if true && !true then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with `and` operator not true && not true"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if !true && !true then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with `and` operator true && not false"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if true && !false then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with `and` operator not true && not false"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if !true && !false then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with `and` operator not false && false "){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if !false && false then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with `and` operator not false && not false "){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if !false && !false then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with `and` operator not false && true "){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if !false && true then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with `and` operator not false && not true "){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if !false && !true then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  //=============== Or Operation Pass ===============
  test("test if statement with `or` operator true || true"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if true || true then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with `or` operator true || false"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if true || false then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with `or` operator false || false"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if false || false then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with `or` operator false || true"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if false || true then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  //=============== Not-Or Operation Pass ===============
  test("test if statement with `or` operator not true || true"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if !true || true then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with `or` operator true || not true"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if true || !true then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with `or` operator not true || false"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if !true || false then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with `or` operator true || not false"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if true || !false then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with `or` operator not false || false"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if !false || false then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with `or` operator false || not false"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if !false || false then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  //=============== Relational Operation Statement Pass ===============
  test("test if statement with int true greaterEqual relational operation "){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if 5 >= 10 then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with int false greaterEqual relational operation "){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if 5 >= 4 then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with true int greater relational operation "){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if 5 > 4 then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with false int greater relational operation "){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if 5 > 5 then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with true int lessEqual relational operation "){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if 5 <= 5 then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with false int lessEqual relational operation "){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if 5 <= 4 then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with true int less relational operation "){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if 5 < 10 then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  test("test if statement with false int less relational operation "){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if 5 < 4 then x = false;
          |}
        """.stripMargin)
    assert(message.isEmpty, "expecting no error")
  }

  //=============== If Statement fail ===============
  test("test if statement int type fail "){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if 5 then x = false;
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting one error")
    assertMessage(message, 0, 11, 5, "type error, expecting 'boolean' found 'int'")
  }

  test("test if statement float type fail "){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if 5.0 then x = false;
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting one error")
    assertMessage(message, 0, 11, 5, "type error, expecting 'boolean' found 'float'")
  }

  test("test if statement neighbour type fail "){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
          |function test(){
          | boolean x = false;
          | if N then x = false;
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting one error")
    assertMessage(message, 0, 11, 5, "type error, expecting 'boolean' found 'neighbour'")
  }

}
