package compiler

/**
  * Created by chowii on 8/10/17.
  */
class CoordinatorExpressionTypeAnalysisTest extends SemanticTests{

  //=============== Coordinator Expression Pass ===============
  test("test coordinator expression [int, int] pass"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
        """.stripMargin)
    assert(message.length === 0, "expecting no error")
  }

  //=============== Coordinator Expression Fail ===============
  test("test coordinator expression [int, float] fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,1.0];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting one error")
    assertMessage(message, 0, 3, 22, "type error, expecting 'int' found 'float'")
  }

  test("test coordinator expression [float, int] fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [1.0,1];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting one error")
    assertMessage(message, 0, 3, 20, "type error, expecting 'int' found 'float'")
  }

  test("test coordinator expression [int, true] fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0,true];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting one error")
    assertMessage(message, 0, 3, 22, "type error, expecting 'int' found 'boolean'")
  }

  test("test coordinator expression [true, int] fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [true, 0];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting one error")
    assertMessage(message, 0, 3, 20, "type error, expecting 'int' found 'boolean'")
  }

  test("test coordinator expression [int, false] fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [0, false];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting one error")
    assertMessage(message, 0, 3, 22, "type error, expecting 'int' found 'boolean'")
  }

  test("test coordinator expression [false, int] fail"){
    val message =
      semanticTestInline(
        """
          |dimension(100,100);
          |neighbourhood N = [false, 0];
          |state {}
          |updater {}
          |mapper{
          |return (1);
          |}
        """.stripMargin)
    assert(message.length === 1, "expecting one error")
    assertMessage(message, 0, 3, 20, "type error, expecting 'int' found 'boolean'")
  }

}
